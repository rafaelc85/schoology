
## Requirements

You must have the requirements below to run this project:

1. Have **Git** installed.
2. Have **Docker** installed.
3. Have **Maven** installed.

---

## How to run

Next, you’ll need the following steps to run the project:

1. First step is to clone repository. You can do by running **git clone https://rafaelc85@bitbucket.org/rafaelc85/schoology.git**
2. cd into **schoology**
3. Build a Docker Image with Maven: **sudo ./mvnw install dockerfile:build**
4. Run the docker image: **docker run -p 8080:8080 -t rafaelc85/schoology**
5. The application is then available on GET **http://localhost:8080?search=text**, where text = text that you want to auto complete 

---

## API Documentation

You can view our documentation and test APIs by accessing **http://localhost:8080/swagger-ui.html**

---

## Curl example to access API

**curl -X GET "http://localhost:8080/?search=over" -H "accept: */*"**

---

## Application Tests

To run the all application tests, use **mvn test**

---

## Docker Utils
 
You can use the following docker commands to check the status of your image, stop or remove it: 

1. Type **sudo docker ps** to check if your image is running
2. To stop the image, type **docker stop 57c125b8ea37**, where 57c125b8ea37 is an example of Container ID, listed in the previous step
3. To delete the container, type **docker rm 57c125b8ea37**, where 57c125b8ea37 is the Container ID

---

## Database Information

I use a H2 Database in this project, because it's a fast In-memory database, open source and it can run embedded.

---


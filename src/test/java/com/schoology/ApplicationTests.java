package com.schoology;

import static org.junit.Assert.*;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.context.SpringBootTest.WebEnvironment;
import org.springframework.boot.test.web.client.TestRestTemplate;
import org.springframework.boot.web.server.LocalServerPort;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.test.annotation.DirtiesContext;
import org.springframework.test.context.junit4.SpringRunner;

@RunWith(SpringRunner.class)
@SpringBootTest(webEnvironment = WebEnvironment.RANDOM_PORT)
@DirtiesContext
public class ApplicationTests {

    @LocalServerPort
    private int port;

    @Autowired
    private TestRestTemplate restTemplate;

    @Test
    public void applicationTestValid() throws Exception {
        String search = "over";
    	ResponseEntity<Object> entity = restTemplate
            .getForEntity("http://localhost:" + this.port + "/?search=" + search, Object.class);
        assertEquals(HttpStatus.OK, entity.getStatusCode());  
    }
    
    @Test
    public void applicationTestBadRequest() throws Exception {
    	ResponseEntity<Object> entity = restTemplate
            .getForEntity("http://localhost:" + this.port + "/", Object.class);
        assertEquals(HttpStatus.BAD_REQUEST, entity.getStatusCode());
    }
    
    @Test
    public void applicationTestNotFound() throws Exception {
    	ResponseEntity<Object> entity = restTemplate
            .getForEntity("http://localhost:" + this.port + "/invalidapi", Object.class);
        assertEquals(HttpStatus.NOT_FOUND, entity.getStatusCode());
    }
    
    @Test
    public void applicationMainTest() {
       Application.main(new String[] {});
    }
   

}

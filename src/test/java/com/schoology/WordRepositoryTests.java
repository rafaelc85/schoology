package com.schoology;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;

import java.util.List;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.test.context.junit4.SpringRunner;

import com.schoology.model.Word;
import com.schoology.repository.WordRepository;

@RunWith(SpringRunner.class)
@DataJpaTest
public class WordRepositoryTests {
	
	@Autowired
    WordRepository wordRepository;
    
    @Test
    public void findTop10ByWordStartingWithIgnoreCaseTestNothingFound() {
    	String search = "wordThatDoesntExists";
    	List<Word> words = wordRepository.findTop10ByWordStartingWithIgnoreCase(search);
    	assertEquals(0, words.size());
    }
    
    @Test
    public void findTop10ByWordStartingWithIgnoreCaseTestValidResults() {
    	String search = "over";
    	List<Word> words = wordRepository.findTop10ByWordStartingWithIgnoreCase(search);
    	assertNotNull(words.get(0));
    	assertTrue(words.get(0).getWord().contains(search));
    }
    
    @Test
    public void saveTest() throws Exception {
    	Word word = new Word();
    	word.setWord("wordSavedTest");
    	wordRepository.save(word);  	
    	assertEquals(wordRepository.findById(word.getWord()).get().getWord(), word.getWord());  
    }
    
}
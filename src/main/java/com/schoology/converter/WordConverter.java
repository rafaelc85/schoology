package com.schoology.converter;

import java.util.ArrayList;
import java.util.List;

import org.springframework.stereotype.Component;

import com.schoology.model.Word;

@Component
public class WordConverter {

	public List<String> wordListToStringList(List<Word> words) {
		List<String> response = new ArrayList<>();
		for (Word word : words) {
			response.add(word.getWord());
		}
		return response;
	}
	
}

package com.schoology.service;

import java.util.List;

public interface SearchService {

	public List<String> search(String search);

}

package com.schoology.service.impl;

import java.util.List;
import java.util.NoSuchElementException;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.schoology.converter.WordConverter;
import com.schoology.model.Word;
import com.schoology.repository.WordRepository;
import com.schoology.service.SearchService;

@Service
public class SearchServiceImpl implements SearchService {
	
	@Autowired
	WordRepository wordRepository;
	
	@Autowired
	WordConverter wordConverter;

	@Override
	public List<String> search(String search) throws NoSuchElementException{
		List<Word> words = wordRepository.findTop10ByWordStartingWithIgnoreCase(search);
		return wordConverter.wordListToStringList(words);
	}

}

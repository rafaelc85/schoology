package com.schoology.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.schoology.service.SearchService;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;

@CrossOrigin
@RestController
@Api(value = "Search API")
public class SearchController {
	
	@Autowired
	SearchService searchService;
	
	@ApiOperation(value = "Search for an autocomplete tool")
    @RequestMapping(path="/", method = RequestMethod.GET)
    public ResponseEntity<Object> search(@RequestParam("search") String search){ 
    	List<String> response = searchService.search(search);
    	return new ResponseEntity<Object>(response, HttpStatus.OK);
    }
    
}


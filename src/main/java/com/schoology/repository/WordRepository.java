package com.schoology.repository;

import java.util.List;

import org.springframework.data.repository.CrudRepository;

import com.schoology.model.Word;

public interface WordRepository extends CrudRepository<Word, String> {
	
    List<Word> findTop10ByWordStartingWithIgnoreCase(String search);
	
}
